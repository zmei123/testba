<?php

use yii\db\Migration;

/**
 * Class m191022_082110_init
 */
class m191022_082110_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('authors', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'birthyear' => $this->integer()->notNull(),
            'rating' => $this->double()->notNull()
        ]);

        $this->createTable('books', [
            'id' => $this->primaryKey(),
            'author_id' => $this->integer()->notNull(),
            'year' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'rating' => $this->double()
        ]);

        $this->addForeignKey('fk-books-authors', 'books', 'author_id', 'authors', 'id', 'CASCADE');

        $this->createIndex('idx-books_author_id', 'books', 'author_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191022_082110_init cannot be reverted.\n";

        $this->dropTable('books');
        $this->dropTable('authors');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191022_082110_init cannot be reverted.\n";

        return false;
    }
    */
}
